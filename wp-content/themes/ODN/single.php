<?php get_header(); ?>
<?php while(have_posts()): the_post(); ?>

<section class="blog-content">
    <div class="container">
        <h1><?= get_the_title(); ?></h1>
        <?= apply_filters('the_content', $post->post_content); ?> 
    </div>
</section>

<?php endwhile; ?>
<?php get_footer(); ?>
