<section class="basic-hero-section flex flex-ac" style="background: url(<?= get_sub_field('basic_hero_bg') ?>) no-repeat center / cover;">
    <div class="container">
        <h1 class="tc-wht"><?= get_sub_field('basic_hero_title') ?></h1>
    </div>
</section>