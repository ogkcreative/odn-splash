</main>
<footer class="footer">
    <div class="footer-top container-large flex flex-sb flex-ac">
        <div class="logo">
            <?= file_get_contents(get_field( 'logo', 'options' )); ?>
        </div>
        <div class="footer-menu flex flex-ac">
            <?php
            wp_nav_menu( array(
                'menu' => 'footer-menu'
            ) );
            ?>
            <div class="buttons flex flex-ac">
                <div class="footer-phone btn-wrap">
                    <a href="tel:<?= get_field('phone', 'options') ?>" class="flex flex-ac">
                        <?= file_get_contents(get_field('phone_icon', 'options')) ?>
                        <?= get_field('phone', 'options') ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom container-large flex flex-sb">
        <div class="footer-copyright">
            <p>©<?= date('Y') ?> | All Rights Reserved | <a href="/terms/">Terms & Conditions</a> | <a href="/privacy-policy/">Privacy Policy</a> | Site Powered by <a href="https://ogkcreative.com/" target="_blank">OGK Creative</a></p>
        </div>
        <?php get_template_part("includes/social-icons"); ?>
    </div>
    <div class="footer-splash">
        <a class="phone" href="tel:<?= get_field('phone', 'options') ?>" class="flex flex-ac">
            <?= get_field('phone', 'options') ?>
        </a>
        <p class="location"><?= get_field('location', 'options')?></p>
        <div class="address tc-lightGray">
            <p><?= get_field('address', 'options')?></p>
            <p>
                <?= get_field('city', 'options')?>
                <?= get_field('state', 'options')?>
                <?= get_field('zip', 'options')?>
            </p>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>

<script defer async="async" src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@12.3.0/dist/lazyload.min.js"></script>
</body>
</html>