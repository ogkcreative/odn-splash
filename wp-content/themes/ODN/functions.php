<?php

/**
 * ALL Non Specific Theme based functions include here
 * if not user /wp-content/plugins/a-custom-functions/a-custom-functions.php file
 **/


// Developer Theme options
// Woocommerce options can be commented out if not required
// To use these inside functions, add "global $theme_setup;"
$theme_setup = array(

    // set some variables to use instead of calling the same function heaps of times (more efficient)
    'template_directory' => get_template_directory(),
    'template_directory_uri' => get_template_directory_uri(),

    // to get an option use ogk_get_get_acf_option($name, $optional_default_value);
    // this is for client options (not development options)
    'add_acf_options_page' => true,

    // Gravity form ID for contact page
    'contact_form_id' => 1,

    'global_search_post_types' => array('post' => 'Blog Posts', 'product' => 'Products', 'page' => 'Pages'),

    //// Disable WP Core features, styles, scripts and header links
    'remove_admin_bar' => true, // for non-admins
    'disable_dash_icon_styles' => true, // if not logged in
    'disable_admin_bar_styles' => true, // if not logged in
    'disable_emoji' => true,
    'disable_block_library' => true, // Gutenberg's block styles
    'disable_rest_api' => true,
    'disable_rsd_link' => true,
    'disable_live_writer_link' => true,
    'disable_shortlinks' => true,
    'disable_wp_generator' => true,
    'disable_jquery_migrate' => true,
    //'disable_wp_embeds' => false, // may cause issues with Woo

    ////// Theme performance enhancement
    // dump critical CSS to the head instead of enqueue
    'inline_critical_css' => true,
    // dump critical JS to the head instead of enqueue
    'inline_critical_js' => true,
    // whether to enqueue the deferred CSS file
    'deferred_css' => true, // several blocks use this, keep enabled if you're not sure.
    // whether to enqueue the deferred JS file
    'deferred_js' => true, // several blocks use this, keep enabled if you're not sure.
    // whether to generate webp CSS when generating css (when using picture and background image functions)
    'conditional_webp' => true,

    // prerender pages into the browsers cache.
    'prerender_pages' => true,
    // menus to grab prerender URIs from
    'prerender_menus' => array( 'primary-nav', 'footer-menu-1'),

    //// transient caching & compiling
    'header_caching' => true, // separate to the ogk cache
    'footer_caching' => true, // separate to the ogk cache

    // OGK caching & compiling
    /*'enable_ogk_cache' => true, // needs more testing
    'cache_directory' => '/ogk_cache',
    'cache_directory_uri' => get_template_directory() . '/ogk_cache',
    'enable_compile_css' => true,
    'enable_compile_js' => true, // DONT USE YET - needs work. jQuery not defined
    // these are set in the theme setup on a per page basis. does not include reserved handles (defer-styles, defer-scrips, admin-bar)
    'enqueued_scripts' => array(),
    'enqueued_styles' => array(),
    'compiled_js_file_name' => '', // don't change this, declaring only
    'compiled_css_file_name' => '', // don't change this, declaring only*/

    ////// Images

    'custom_image_sizes' => array(
        'ogk_x_large' => array(
            'name' => 'OGK X Large',
            'width' => 1300,
            'height' => ''
        ),
        'ogk_large' => array(
            'name' => 'OGK Large',
            'width' => 1024,
            'height' => ''
        ),
        'ogk_medium' => array(
            'name' => 'OGK Medium',
            'width' => 768,
            'height' => ''
        ),
        'ogk_small' => array(
            'name' => 'OGK Small',
            'width' => 480,
            'height' => ''
        ),
    ),

    /////// CSS

    'css_breakpoints' => array(
        'large_desktop' => array(
            'name' => 'Large Desktop',
            'max_width' => 1300,
            'main_image' => 'OGK X Large',
            'retina_image' => 'full'
        ),
        'small_desktop' => array(
            'name' => 'Small Desktop',
            'max_width' => 1024,
            'main_image' => 'OGK Large',
            'retina_image' => 'full'
        ),
        'tablet' => array(
            'name' => 'Tablet',
            'max_width' => 768,
            'main_image' => 'OGK Medium',
            'retina_image' => 'full'
        ),
        'mobile' => array(
            'name' => 'Mobile',
            'max_width' => 480,
            'main_image' => 'OGK Small',
            'retina_image' => 'OGK Large'
        ),
    ),

    // login Modal redirect URL
    // redirects to the Woo account area is Woo enabled, otherwise to the current page
    'login_modal_redirect' => (class_exists('WooCommerce')) ? wc_get_page_permalink('myaccount') :
        (is_ssl() ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'],

    // add extra menu locations ,  5 included by default (primary, mobile, top-bar, footer1, footer2)
    'menu_locations' => array(
        'primary-nav' => 'Primary Navigation',
        'mobile-nav' => 'Mobile Navigation',
        'top-bar-menu' => 'Top Bar Menu',
        'my_account_menu' => 'My Account Menu',
        'footer-menu-1' => 'Footer Menu 1',
        'footer-menu-2' => 'Footer Menu 2',
        'footer-menu-3' => 'Footer Menu 3',
        'footer-bottom-bar-menu' => 'Footer Bottom Bar Menu',
    ),

    // Post types
    'custom_post_types' => array(/*'videos' => array(
      'singular' => 'Video',
      'plural' => 'Videos',
      'taxonomies' => array('video_type'),
    ),*/

    ),

    // enable all theme Woocommerce features
    'woocommerce_enabled' => true,

    // disable the default Woocommerce styles
    'woocommerce_disable_styles' => true,

    // number of posts to display per page on the archive. used by ajax (filters, lazy loading) too
    'woocommerce_products_per_page' => 1,

    // Woocommerce theme support options
    'woocommerce_theme_support_options' => array(
        'thumbnail_image_width' => 150,
        'single_image_width' => 300,
        'product_grid' => array(
            'default_rows' => 3,
            'min_rows' => 2,
            'max_rows' => 8,
            'default_columns' => 4,
            'min_columns' => 2,
            'max_columns' => 5,
        ),
    ),

    // Woocommerce Gallery theme support
    'woocommerce_gallery_features' => array(
        'wc-product-gallery-zoom',
        'wc-product-gallery-lightbox',
        /*'wc-product-gallery-slider'*/
    ),

);




/** =========================================================================================== **/

require_once($theme_setup['template_directory'] . '/assets/php/ogk-base-functions.php');
require_once($theme_setup['template_directory'] . '/assets/php/ogk-enqueue.php');
require_once($theme_setup['template_directory'] . '/assets/php/media-functions.php');

/** =========================================================================================== **/