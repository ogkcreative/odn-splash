<?php if(have_rows('social_links','options')): ?>
<div class="social">
	<?php while(have_rows('social_links','options')): the_row(); ?>
	<a href="<?php the_sub_field('url'); ?>" target="_blank">
		<?php if(!get_sub_field('custom_icon')): ?>
		<i class="fa fa-<?= strtolower(the_sub_field('site')) ?> " aria-hidden="true"></i>
		<?php else: ?>
		<img src="<?= the_sub_field('custom_icon') ?>" alt="<?= the_sub_field('site') ?>" >
		<?php endif; ?>
	</a>
	<?php endwhile; ?>
</div><!--/social-->
<?php endif; ?>