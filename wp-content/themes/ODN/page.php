<?php get_header(); ?>

<section class="default-section">
    <div class="container">
        <?php if(have_posts()): while(have_posts()): the_post(); ?>
            <?php the_content() ?>
        <?php endwhile; endif; ?>
    </div>
</section>

<?php get_template_part("includes/flexible"); ?>

<?php get_footer(); ?>
