#OGK Base Functions
This plugin is used to provide a base framework of PHP functions, JS Functions and to pull in vendor files via NPM and composer.

 ![[OGK Creative](https://ogkcreative.com)](https://img.shields.io/badge/OGKCreative-2019-lightgrey.svg)
 
#####Plugins:
 ![plugins](https://img.shields.io/wordpress/plugin/v/advanced-custom-fields.svg?label=Advanced%20Custom%20Fields&style=flat)
 ![plugins](https://img.shields.io/wordpress/plugin/v/woocommerce.svg?label=Woocoomerce&style=flat)
  ![plugins](https://img.shields.io/wordpress/plugin/v/gravityforms.svg?label=Gravity%20Forms&style=flat)
 
 
#####JS Dependencies:
<small>_Installed via NPM:_</small><br>
![jquery](https://img.shields.io/npm/v/jquery.svg?label=jquery&style=flat)
![inview](https://img.shields.io/npm/v/jquery-inview.svg?label=jquery-inview&style=flat)
![cross-env](https://img.shields.io/npm/v/cross-env.svg?label=cross-env&style=flat)
![font-awesome](https://img.shields.io/npm/v/font-awesome.svg?style=flat&label=font-awesome)
![lazy-sizes](https://img.shields.io/npm/v/lazysizes.svg?style=flat&label=lazy-sizes)
![lity](https://img.shields.io/npm/v/lity.svg?style=flat&label=lity)
![owl-carousel](https://img.shields.io/npm/v/owl-carousel.svg?style=flat&label=owl-carousel)


##CLASS USAGES
###Namespace : OGK 

#####Classes:
```php
Auth
Base
Post
Query
Utlities
Image
Video

```

#####Traits:
```php
ImageTrait
```

####Usage
```php
namespace OGK

$query = new OGK\Query;
$query->get_all('post');  /* Gets all posts to any array  */

```

##Compiling

to compile assets open a terminal window and navigate to the `ogk-base-functions` plugin folder.  From the root of the folder run the below npm commands 


```bash
#See 'webpack.mix.js' file for steps

npm run dev  #compile for development
npm run prod  #compile for production
npm run watch  #hotpatch watch for file changes
```
 <small>[![webpack](https://img.shields.io/npm/v/webpack.svg?style=flat&label=webpack)](https://webpack.js.org/) 

##Contributing
Must be a developer employed at OGK Creative to pull this resource

## License
![[MIT](https://choosealicense.com/licenses/mit/)](https://img.shields.io/badge/license-MIT-brightgreen.svg)



